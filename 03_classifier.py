from pathlib import Path
import re

# For data handling
import scipy.sparse
import numpy as np
import pandas as pd
import datasets
from datasets import DatasetDict

# For language processing and word embeddings
import spacy
import gensim
import gensim.downloader as gensim_api
from tensorflow.keras import preprocessing as keras_processing

# For preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import RandomForestClassifier

# For classification
from sklearn.svm import LinearSVC, SVC
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

# For (custom) pipeline components
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, TransformerMixin

# Evaluation
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
from sklearn import metrics
from copy import deepcopy

# Miscellaneous
from miscellaneous import VerbosePrinter
import argparse
from contextlib import redirect_stdout, redirect_stderr


# === Data Handling ===

class FileNames:
    # Source file
    src_orig = Path("inferred/01_preprocessing/clf_qa_pairs.csv")
    src_gpt = Path("inferred/02_gpt2/generated_answers")
    # Inf directory
    inf_dir = Path("inferred/03_classifier/")
    inf_dir.mkdir(parents=True, exist_ok=True)


class DataGetter(VerbosePrinter):
    def get_dataset(self, data_src: Path = None, train_size=.8, select_n=None) -> DatasetDict:
        """Read *.csv file(s) from `data_src` and prepare for classifier.
        
        :param data_src: path to folder or file (columns: `question`, `party`, `answer`)
        :param train_size: ratio of train-test-split
        :param select_n: randomly select only `n` rows
        :returns: dataset for classifier
        """
        
        # Get file name(s)
        if str(data_src).lower().endswith(".csv"):
            file_names = [str(data_src)]
        else:
            file_names = [str(path) for path in Path(data_src).glob("*.csv")]
        if len(file_names) == 0:
            raise ValueError("No *.csv file path found")
        
        # Read data
        self.print("Read data")
        data = datasets.load_dataset("csv", data_files=file_names, split="train", sep=";")
        
        # Create label encoding
        self.print("Encode labels", indent=1)
        data = data.map(lambda datum: {"label_enc": 0 if datum["party"] == "National" else 1})
        
        # Cleanup
        self.print("Cleanup data (filter and truncate)", indent=1)
        data = data.map(lambda datum: self._answer_cleanup(datum, "answer", max_length=512))
        n_before = len(data)
        data = data.filter(lambda datum: 16 < len(datum["answer"]))  # too small
        n_after = len(data)
        self.print(f"{n_before - n_after} rows removed. Reason: less than 16 characters", indent=2)

        # Shuffle and select
        self.print("Shuffle", indent=1)
        data = data.shuffle(seed=42)
        if isinstance(select_n, int):
            self.print(f"Sample to (max.) n={select_n} rows", indent=1)
            data = data.train_test_split(train_size=select_n)["train"]
        
        # Split into train and test dataset
        self.print("Train test split", indent=1)
        data = data.train_test_split(train_size=train_size, seed=420)
        # Info: value_counts of labels for train/test set
        train_count = pd.Series(data["train"]["party"]).value_counts()
        test_count = pd.Series(data["test"]["party"]).value_counts()
        self.print("National | train:", train_count.loc["National"],
                   "| test:", test_count.loc["National"], indent=2)
        self.print("Labour   | train:", train_count.loc["Labour"],
                   "| test:", test_count.loc["Labour"], indent=2)
        self.print()
        
        return data
    
    @staticmethod
    def _answer_cleanup(datum: dict, answer_col: str, max_length: int) -> dict:
        """Rough cleanup. Filter end-of-text token and limit length."""
        
        answer = str(datum[answer_col])
        answer = re.sub(r"<[^<>]*>", "", answer)  # remove <|endoftext|>
        answer = answer[:max_length]  # truncate to limit length
        answer = re.sub(r"\s[^\s]*$", "", answer.strip())  # remove (unfinished) last word
        # unfinished last sentences do not matter
        
        return {"answer": answer}


# === Classification ===

class SpacyCleaner(BaseEstimator, TransformerMixin):
    """Use spaCy to lemmatize text and filter out stop words."""
    
    def __init__(self):
        super().__init__()
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.transform(X)
    
    def transform(self, X):
        X = pd.Series(X).apply(lambda x: str(x).lower().strip())  # noqa
        nlp = spacy.load("en_core_web_sm")
        docs = list(nlp.pipe(X, disable=["tagger", "parser", "ner"]))
        docs = pd.Series(docs)
        X = pd.Series(docs).apply(self._spacy_process)  # noqa
        return X.tolist()
    
    @staticmethod
    def _spacy_process(doc) -> str:
        valid_tokens = []
        for token in doc:
            if token.is_stop:
                continue
            elif token.is_currency or str(token) == "dollar":
                valid_tokens.append("_CURR_")
            elif token.like_num:  # re.match(r"^[0-9,]*$", str(token)):
                valid_tokens.append("_NUM_")
            else:
                valid_tokens.append(token.lemma_)  # lemmatized word
        
        # Revert list to string of valid, lemmatized words
        lemmas = " ".join(valid_tokens)
        return lemmas


class SparseToDenseArray(BaseEstimator, TransformerMixin):
    
    def fit(self, X, y=None):
        return self
    
    @staticmethod
    def transform(X):
        print("..  shape:", X.shape)
        if isinstance(X, np.ndarray):
            return X
        return X.toarray()
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, y).transform(X)


class TfidfWord2vec(BaseEstimator, TransformerMixin):
    """Vectorization of text according to
    https://towardsdatascience.com/text-classification-with-nlp-tf-idf-vs-word2vec-vs-bert-41ff868d1794
    """
    
    def __init__(self, random_state=1):
        super().__init__()
        self._random_state = random_state
        self._vec_size = 300
        self._vocabulary = {}
        self._nlp = None
        self._idf = None
        self._embedding = None
    
    def fit(self, X, y=None):
        # Create vocabulary
        tokenizer = keras_processing.text.Tokenizer(lower=True, split=" ", oov_token=None)
        tokenizer.fit_on_texts(X)
        self._vocabulary = tokenizer.word_index
        self._vocabulary["NaN"] = 0  # add oov_token
        
        # Get inverse document frequency
        tf_idf = TfidfVectorizer(vocabulary=self._vocabulary)
        tf_idf.fit(X)
        self._idf = tf_idf.idf_
        
        # Create our own Word2vec
        # self._nlp = gensim.models.word2vec.Word2Vec(
        #     X, size=self._vec_size, window=8, min_count=10, sg=1, iter=30, seed=self._random_state
        # )
        # Use pretrained Word2vec
        self._nlp = gensim_api.load("word2vec-google-news-300")
        
        # Create `word -> word2vec` embedding
        self._embedding = np.zeros(shape=(len(self._vocabulary), self._vec_size))
        for word, idx in self._vocabulary.items():
            try:
                self._embedding[idx] = self._nlp[word]  # word vector
            except:  # noqa
                continue
        
        return self
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X).transform(X)
    
    def transform(self, X):
        # Get word vectors, weighted by tf-idf
        X_vec = np.zeros(shape=(len(X), self._vec_size))
        for idx, text in enumerate(X):
            vector = np.zeros(shape=(self._vec_size,))
            N = 0
            for word in text.split(" "):
                if word in self._vocabulary:
                    N += 1
                    i = self._vocabulary[word]
                    vector += self._idf[i] * self._embedding[i]
                else:
                    continue
            if N != 0:
                vector /= N
            X_vec[idx] = vector
        return X_vec


class TextVectorizer(BaseEstimator, TransformerMixin):
    """According to Support Vector Machines and Word2vec for Text Classification with Semantic Features
    by J. Lilleberg, et. al.
    """
    
    def __init__(self, use_tfidf: bool = True, use_word2vec: bool = False,
                 params_tfidf=None, params_word2vec=None):
        super().__init__()
        if params_tfidf is None:
            params_tfidf = {}
        if params_word2vec is None:
            params_word2vec = {}
        self._use_tfidf = use_tfidf
        self._use_word2vec = use_word2vec
        self._TfidfVectorizer = TfidfVectorizer(**params_tfidf)
        self._Word2Vec = TfidfWord2vec(**params_word2vec)
    
    def fit(self, X, y=None):
        if self._use_tfidf:
            self._TfidfVectorizer.fit(X, y)
        if self._use_word2vec:
            self._Word2Vec.fit(X, y)
        return self
    
    def transform(self, X):
        if self._use_tfidf and self._use_word2vec:
            X_tfidf = self._TfidfVectorizer.transform(X)
            X_word2vec = self._Word2Vec.transform(X)
            # Sparse representation
            X = scipy.sparse.hstack([X_tfidf, X_word2vec])
            # Dense representation
            # X = np.concatenate((X_tfidf.toarray(), X_word2vec), axis=1)
        elif self._use_tfidf:
            X = self._TfidfVectorizer.transform(X)
        elif self._use_word2vec:
            X = self._Word2Vec.transform(X)
        else:
            raise ValueError("Nothing to transform. Set `use_tfidf` and/or `use_word2vec` as True.")
        return X
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, y).transform(X)


class BaseClassifier(VerbosePrinter):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.X_train = None
        self.X_test = None
        self.y_train = None
        self.y_test = None
        self.y_test_pred = None
    
    def fit_predict_datadict(self, dataset: DatasetDict, mode="random", *args, **kwargs):
        self.print("Base classification...")
        self.print("mode", mode, indent=1)
        
        self._set_data(dataset)
        length = len(self.y_test)
        
        if mode == "random":
            np.random.seed(420)
            self.y_test_pred = np.random.randint(2, size=length)
        elif mode == "ones":
            self.y_test_pred = np.ones(shape=length)
        elif mode == "zeros":
            self.y_test_pred = np.zeros(shape=length)
        else:
            raise NotImplementedError(f"mode={mode} is not implemented")
        
        return self.y_test_pred
    
    def _set_data(self, dataset: DatasetDict):
        train = dataset["train"]
        test = dataset["test"]
        self.X_train = train["answer"]
        self.X_test = test["answer"]
        self.y_train = np.array(train["label_enc"], dtype=int)
        self.y_test = np.array(test["label_enc"], dtype=int)
        return self
    
    # --- Evaluation ---
    
    def evaluate(self, y_pred, y_true, store_dir: Path):
        self.store_y_pred_and_true(y_pred, y_true, store_dir)
        self.print_classification_report()
        return self
    
    def store_y_pred_and_true(self, y_pred, y_true, store_dir: Path):
        pd.Series(y_pred).rename("y_pred").to_csv(store_dir / "y_pred.csv", index=False, header=True)
        pd.Series(y_true).rename("y_true").to_csv(store_dir / "y_true.csv", index=False, header=True)
        return self
    
    def print_classification_report(self):
        report = classification_report(self.y_test, self.y_test_pred)
        self.print("Classification report:")
        print(report)
        return self


class TextClassifier(BaseClassifier):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pipe = None
        self._params = None
    
    # --- MAIN ---
    
    def fit_predict_datadict(self, dataset: DatasetDict, params: dict = None, **kwargs):
        self.print("Classification...")
        
        self._set_data(dataset)
        self._params = params
        self._pipe = self._create_pipeline(**kwargs)
        
        self.print("Train classifier...", indent=1)
        self._pipe.fit(self.X_train, self.y_train)
        self.y_test_pred = self._pipe.predict(self.X_test)
        
        return self.y_test_pred
    
    def _create_pipeline(self, use_tfidf=True, use_word2vec=False) -> Pipeline:
        self.print("Create pipeline", indent=1)
        
        pipe = Pipeline([
            ("text_cleaner", SpacyCleaner()),
            ("feature_creation", TextVectorizer(
                use_tfidf=use_tfidf,
                use_word2vec=use_word2vec,
                params_tfidf=dict(ngram_range=(1, 3)),
                params_word2vec=dict(),
            )),
            ("feature_selection", SelectFromModel(RandomForestClassifier(
                # max_features="log2",
                # n_estimators=50,
                n_jobs=-1,
                random_state=420,
                ),
                max_features=1_000,  # use max 1000 features for classification
            )),
            ("sparse_to_dense", SparseToDenseArray()),
            ("scaling", StandardScaler(copy=False, with_mean=True)),
        ], verbose=self._verbose)

        if self._params is not None:
            pipe.steps.append(["classifier", SVC(**self._params)])
        else:
            self._use_fixed_params = True
            param_grid = {
                "C": np.linspace(10, 1e4, 4),
                "kernel": ["poly", "rbf"],
                "degree": [3, 4],
                "tol": np.linspace(1e-6, 1e-3, 4),
                "class_weight": ["balanced"],
                "random_state": [69],
            }
            pipe.steps.append(["classifier_cv", RandomizedSearchCV(
                SVC(), param_grid, cv=5,
                scoring="accuracy", refit=True,  n_jobs=-1,
                verbose=1
            )])
        
        return pipe
    
    # --- Evaluation ---
    
    def evaluate(self, y_pred, y_true, store_dir: Path):
        self.print()
        self.print("Evaluation...")
        self.print()
        self.store_y_pred_and_true(y_pred, y_true, store_dir)
        self.print_pipe_info()
        self.print_classification_report()
        self.plot_roc_curve(store_dir)
        return self
    
    def print_pipe_info(self):
        self.print("Pipe info:")
        pipe = deepcopy(self._pipe)
        
        if self._params is None:
            self.print("Best parameter (CV score=%0.3f):" % pipe.steps[-1][1].best_score_, indent=1)
            self._params = pipe.steps[-1][1].best_params_
        else:
            self.print("Used parameters:", indent=1)
        self.print(self._params, indent=2)
        
        try:
            pipe.steps.pop(-1)  # skip classification
            self.print("# columns w/ feature selection:", pipe.transform(self.X_train[:1]).shape[1], indent=1)
            pipe.steps.pop(-1)  # skip standard scaler
            pipe.steps.pop(-1)  # skip sparse to dense transformer
            pipe.steps.pop(-1)  # skip feature selection
            self.print("# columns w/o feature selection:", pipe.transform(self.X_train[:1]).shape[1], indent=1)
        except:
            self.print("Sorry. Couldn\'t print more pipe info...")
        
        return self
    
    def plot_roc_curve(self, store_dir: Path):
        self.print("Plotting ROC curve...")
        
        fig, ax = plt.subplots(figsize=(7, 5))
        metrics.plot_roc_curve(self._pipe, self.X_test, self.y_test, ax=ax)
        # plt.show()
        fig.savefig(store_dir / 'roc_curve.png', dpi=fig.dpi)
        
        return self


# === MAIN ===

def main(data_src="orig", tfidf=True, word2vec=True, store_dir=None, verbose=0) -> None:
    # Choose dataset source
    f_names = FileNames()
    if data_src in ("orig", "original"):
        data_src = f_names.src_orig
    elif data_src in ("gpt", "gpt_output"):
        data_src = f_names.src_gpt
    else:
        raise ValueError(f"Invalid option `data_src={data_src}`.")
    
    # Get data
    dataset = DataGetter(verbose=verbose).get_dataset(data_src, select_n=19_000)
    
    # Classification
    if tfidf or word2vec:
        model = TextClassifier(verbose=verbose)
        clf_params = {'C': 10.0, 'degree': 3, 'kernel': 'rbf', 'random_state': 69, 'tol': 1e-06}
        # clf_params = None  # make cross validation
        model_params = dict(use_tfidf=tfidf, use_word2vec=word2vec, params=clf_params)
    else:
        model = BaseClassifier(verbose=verbose)
        model_params = dict(mode="random")
    
    y_pred = model.fit_predict_datadict(dataset, **model_params)
    y_true = model.y_test
    
    # Evaluation
    model.evaluate(y_pred, y_true, store_dir)
    
    return


def _init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTIONS...]",
        description="Perform binary text classification on given dataset."
    )
    parser.add_argument("-d", "--data_src", nargs="?", default="orig",
                        help="choose dataset source")
    parser.add_argument("--tfidf", action="store_true",
                        help="use TF-IDF text vectorization")
    parser.add_argument("--word2vec", action="store_true",
                        help="use Word2vec weighted by TD-IDF text vectorization")
    parser.add_argument("-v", "--verbose", action="count", default=0,
                        help="verbosity")
    return parser


if __name__ == '__main__':
    # Init argparse
    arg_parser = _init_argparse()
    args = vars(arg_parser.parse_args())

    # Define store path
    dir_name = str(args["data_src"])
    if not args["tfidf"] and not args["word2vec"]:
        dir_name += "_random"
    else:
        dir_name += "_tfidf" if args["tfidf"] else ""
        dir_name += "_word2vec" if args["word2vec"] else ""
    directory = FileNames().inf_dir / dir_name
    directory.mkdir(parents=True, exist_ok=True)
    
    # Redirect all prints to file
    with open(directory / "_terminal_out_.txt", "w") as f:
        with redirect_stdout(f), redirect_stderr(f):
            print("=" * 30)
            print("ArgumentParse arguments:", args)
            print("=" * 30, "\n")
            # Execute
            main(**args, store_dir=directory)

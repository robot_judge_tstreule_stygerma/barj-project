#!/bin/bash

#Define some colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#Simple function to print status updates in green 
print_status() {
	echo -e "${GREEN}$1 \n ${NC}"
}

print_error() {
	echo -e "${RED}$1 \n ${NC}"
}


#Define needed variables
download_model=false
finetune=false
dependencies=false
generation=false
iteration=0
re='^[0-9]+$'
package="02_automate_generation.sh"

#Manage command line arguments
while test $# -gt 0; do
  	case "$1" in
    	-h|--help)
      		echo "This bash file is a helper file to finetune the GPT-2 model"
      		echo "as well as generating answers with GPT"
      		echo " "
      		echo "$package [options] "
      		echo " "
      		echo "options:"
      		echo "-h, --help        Show brief help"
			echo " "
      		echo "-m,               Download the gpt-2 model with 355M parameters"
			echo " "
      		echo "-f,               Finetune the model."
			echo -e "                  ${RED}NOTE:${NC} This process should be executed on ETH's Leonhard "
			echo "                  cluster."
			echo " "
      		echo "-i                Use this when the finetuning should not continue for "
			echo "                  2000 iterations. Include a positve integer from 0 to"
      		echo "                  10, default is 0. The model will be finetuned for " 
			echo "                  (10-i)*200 steps starting from the latest checkpoint"
			echo "                  or if none is available from the general GPT model that" 
			echo "                  was not finetuned."
			echo " "
      		echo "-d                Download dependencies. Should be provided when running "
      		echo "                  for the first time one this server."
			echo " "
			echo "-g                Generate answers with a fine-tuned GPT-2 model. The finetuned"
			echo "                  GPT model has to be stored in the "
			echo "                  \"checkpoint/NZ_Parliament_QA\" folder either fine-tune a "
			echo "                  model yourself or use the provided checkpoint stored in the "
			echo "                  gitlab repository and unzip it into the correct folder."
			echo -e "                  ${RED}NOTE:${NC} This process should be executed on ETH's Leonhard cluster."
      		exit 0
      		;;
    	-m)
      		shift
      		download_model=true
      		;;
    	-f)
      		shift 
      		finetuning=true
      		;;
    	-d)
      		shift
      		dependencies=true	
      		;;
    	-i)
      		shift 
      		if test $# -gt 0; then
      			iteration=$1
      			shift
      			if ! [[ "$iteration" =~ $re ]] ; then
					print_error "Need to provide a positive integer with the -i flag"  
					exit 1
				fi
	  		else 
	  			print_error "Need to provide a positive integer with the -i flag" 
				exit 1
	  		fi
	  		;;
		-g)
			shift
			generation=true 
	  		;;
  	esac
done


	

#Download necessary libraries and modules
if [ "$dependencies" = true ]; then
	module load gcc/6.3.0 python_cpu/3.7.4 python_gpu/3.7.4
	python -m pip install --user --upgrade pip
	python -m pip uninstall tensorflow tensorflow-tensorboard tensorflow-estimator
	module load eth_proxy
	python -m pip install tensorflow==1.15
	python -m pip install gpt-2-simple
	python -m pip install tensorflow-gpu==1.15
	python -m pip install pandas
	print_status "Downloaded necessary libraries and modules"
else
	print_status "Did not download any dependencies"
fi

#Download 335M gpt-2-model if the -m flag is set 
if [ "$download_model" = true ]; then
	print_status "Download GPT-2 model with 355M parameters"
	wget https://raw.githubusercontent.com/openai/gpt-2/master/download_model.py
	python download_model.py 355M 
	rm download_model.py
	print_status "Downloaded GPT-2 model with 355M parameters and stored in the \"models\" directory"
else 
	print_status "GPT-2 model will not be downloaded"
fi


#Finetuning of the model is executed when the -f flag is set and the starting iteration is given over the -i flag
if [ "$finetuning" = true ]; then 
	print_status "Finetuning selected"

	#Create the necessary directories and download needed files
	#Create inferred directory if it does not exist 
	INF="inferred"
	if [ ! -d "$INF" ]; then 
		mkdir $INF
		print_status "Created \"inferred\" directory"
	fi
	#Create inferred/01_preprocessing directory if it does not exist 
	PREPR="inferred/01_preprocessing"
	if [ ! -d "$PREPR" ]; then 
		mkdir $PREPR
		print_status "Created \"inferred/01_preprocessing\" directory"
	fi
	#Finetuning dataset
	FT="gpt_finetune.txt"
	if [ ! -f "$PREPR/$FT" ]; then 
		wget -P $PREPR https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/inferred/01_preprocessing/gpt_finetune.txt
		print_status "Downloaded \"gpt_finetune.txt\" file and stored in $INF directory" 
	fi
	#Finetuning python file
	FT_PY="02_gpt_finetuning.py"
	if [ ! -f "$FT_PY" ]; then 
		wget https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/02_gpt_finetuning.py
		print_status "Downloaded \"02_gpt_finetuning.py\" file"
	fi

	
	if ! [[ "$iteration" =~ $re ]] ; then
		print_error "Input the starting iteration by using the -i flag"
		exit 1
	else
		#Finetune the model over (10-iteration)*200 iterations using Leonhards hardware
		echo -e "Finetuning with checkpoints saved every 200 iterations" >> .finetuning_log.txt
		max=$((10-$iteration))
		for j in $(seq 0 $max); do
			echo -e "\n===========================================================\nIteration $(($j + $iteration)) started:" >> .finetuning_log.txt
			bsub -n 10 -R "rusage[mem=12800,ngpus_excl_p=8]" python 02_gpt_finetuning.py >> finetuning_log.txt 
		done
	fi
else 
	print_status "Finetuning was not selected"	
fi
	
	
	

if [ "$generation" = true ]; then
	print_status "Generation step selected"

	#Create the necessary directories and download needed files
	#Create inferred directory if it does not exist 
	INF="inferred"
	if [ ! -d "$INF" ]; then 
		mkdir $INF
		print_status "Created \"inferred\" directory"
	fi
	#Create inferred/01_preprocessing directory if it does not exist 
	PREPR="inferred/01_preprocessing"
	if [ ! -d "$PREPR" ]; then 
		mkdir $PREPR
		print_status "Created \"inferred/01_preprocessing\" directory"
	fi
	#Create inferred/02_gpt2 directory if it does not exist
	GPT="inferred/02_gpt2"
	if [ ! -d "$GPT" ]; then 
		mkdir $GPT
		print_status "Created \"inferred/02_gpt2\" directory"
	fi
	#Generation dataset
	GEN="gpt_generate.csv"
	if [ ! -f "$PREPR/$GEN" ]; then
		wget -P $PREPR https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/inferred/01_preprocessing/gpt_generate.csv
		print_status "Downloaded \"gpt_generate.csv\" file and stored in $INF directory"
	fi
	#Generation python file
	GEN_PY="02_generating_answers.py"
	if [ ! -f "$GEN_PY" ]; then
		wget https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/blob/master/02_generating_answers.py
		print_status "Downloaded \"02_generating_answers.py\" file"
	fi
	#Create checkpoint directory if it does not exist
	CHK="checkpoint"
	if [ ! -d "$CHK" ]; then 
		mkdir $CHK
		print_status "Created \"checkpoint\" directory"
	fi
	#Create parliament subfolder if it does not exist
	RUN_NAME="NZ_Parliament_QA"
	if [ ! -d "$CHK/$RUN_NAME" ]; then 
		mkdir $CHK/$RUN_NAME
		print_status "Created \"checkpoint/NZ_Parliament_QA\" directory"
	fi
	#Check if there are any files in the defined directory, if not print error message and exit program
	if ! [ "$(ls -A $CHK/$RUN_NAME)" ]; then
		print_error "Provide a valid GPT-2 checkpoint and store it in the \"checkpoint/NZ_Parliament_QA\" directory, a working example is given at https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/blob/master/inferred/02_gpt2/latest_checkpoint.zip download it and unzip it into the mentioned directory"
		exit 1
	fi

	#Generate answers for all the question given. This handles 20 questions at once and runs in parallel on the Leonhard hardware. Answers are stored in the inferred/02_gpt folder
	for i in $(seq 0 968); do 
		from=$(( 10*i ))
		to=$(( 10*i + 10))
		bsub -n 2 -R "rusage[mem=12800,ngpus_excl_p=1]" python 02_generating_answers.py $from $to
		echo -e "\n===========================================================\nIteration from $from to $to finished:" >> .generation_log.txt
	done
	from=9690
	to=9697
	bsub -n 2 -R "rusage[mem=12800,ngpus_excl_p=1]" python 02_generating_answers.py $from $to 
	echo -e "\n===========================================================\nIteration from $from to $to finished:" >> .generation_log.txt
else 
	print_status "Generation step was not selected"
fi

#Can exit the program without error
exit 0
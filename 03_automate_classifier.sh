#!/bin/bash

Define some colors
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

#Simple function to print status updates in green 
print_status() {
	echo -e "${GREEN}$1 \n ${NC}"
}

print_error() {
	echo -e "${RED}$1 \n ${NC}"
}


initialization=false
package=automate_classifier
#Manage command line arguments
while test $# -gt 0; do
  	case "$1" in
    	-h|--help)
       		echo "This bash file is a helper file for the classification"
      		echo " "
      		echo "$package [options] "
      		echo " "
      		echo "options:"
      		echo "-h, --help        Show brief help"
			echo " "
            echo "-i                Run this file with this flag first in"
            echo "                  order to initialize the directory "
            echo "                  and to download the necessary files"
            exit 0
        ;;
        -i)
            shift 
            initialization=true
            ;;
    esac
done


if [ "$initialization" = true ]; then
    print_status "Initialization selected"

    #Create the necessary directories and download needed files
	#Create inferred directory if it does not exist 
    INF="inferred"
	if [ ! -d "$INF" ]; then 
		mkdir $INF
		print_status "Created \"inferred\" directory"
	fi
    #Create inferred/01_preprocessing directory if it does not exist 
	PREPR="inferred/01_preprocessing"
	if [ ! -d "$PREPR" ]; then 
		mkdir $PREPR
		print_status "Created \"inferred/01_preprocessing\" directory"
	fi
    #Download original qa dataset
	ORIG="clf_qa_pairs.csv"
	if [ ! -f "$PREPR/$ORIG" ]; then
		wget -P $PREPR https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/inferred/01_preprocessing/clf_qa_pairs.csv
		print_status "Downloaded \"clf_qa_pairs.csv\" file and stored in $PREPR directory"
	fi

    #Create inferred/02_gpt2 directory if it does not exist
	GPT="inferred/02_gpt2"
	if [ ! -d "$GPT" ]; then 
		mkdir $GPT
		print_status "Created \"inferred/02_gpt2\" directory"
	fi

    

    #Download classifier python file
	CLF_PY="03_classifier.py"
	if [ ! -f "$CLF_PY" ]; then
		wget https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/03_classifier.py
		print_status "Downloaded \"03_classifier.py\" file and stored in current directory"
	fi

    #Download miscellaneous python file
	MIS_PY="miscellaneous.py"
	if [ ! -f "$MIS_PY" ]; then
		wget https://gitlab.com/robot_judge_tstreule_stygerma/barj-project/-/raw/master/miscellaneous.py
		print_status "Downloaded \"miscellaneous.py\" file and stored in current directory"
	fi

    #Create inferred/02_gpt2 directory if it does not exist
	GEN="inferred/02_gpt2/generated_answers"
	if [ ! -d "$GEN" ]; then 
		mkdir $GEN
		print_status "Created \"inferred/02_gpt2/generated_answers\" directory. NOTE: this is the location for the generated answers"
	fi


    print_error "Provide all the generated answers in the $GEN folder"
    exit 0
fi

# Install dependencies for 03_classifier.py

module load gcc/6.3.0 python_gpu/3.7.4
python3 -m pip install --user --upgrade pip
python3 -m pip install scipy numpy pandas
python3 -m pip install datasets
python3 -m pip install spacy gensim
python3 -m pip install tensorflow sklearn
python3 -m pip install matplotlib


# Get access to internet

module load eth_proxy


# Execute all combinations of 03_classifier.py

bsub -n 1 -R "rusage[mem=10000]" python3 03_classifier.py -d orig -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d orig --tfidf -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d orig --word2vec -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d orig --tfidf --word2vec -vvv

bsub -n 1 -R "rusage[mem=10000]" python3 03_classifier.py -d gpt -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d gpt --tfidf -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d gpt --word2vec -vvv
bsub -n 4 -R "rusage[mem=10000]" python3 03_classifier.py -d gpt --tfidf --word2vec -vvv


# Alternative

# bsub -n 4 -R "rusage[mem=10000]" -W 8:00 python3 03_classifier.py -d original --tfidf -vvv

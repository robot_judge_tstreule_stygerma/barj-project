"""
This module incorporates the preprocessing for the dataset, containing political
speeches from New Zealand's House of Representatives.
"""

from pathlib import Path
import re

import spacy
from spacy.tokens import Doc
from spacy_langdetect import LanguageDetector

import numpy as np
import pandas as pd
from tqdm import tqdm  # progress bar
# from pandarallel import pandarallel

from miscellaneous import VerbosePrinter

tqdm.pandas()
# pandarallel.initialize(progress_bar=True)


# === Primitives ===

class FileNames:
    """Includes all file names for this module."""
    
    # Hidden
    _SRC_DIR = Path("data")
    _INF_DIR = Path("inferred") / "01_preprocessing"
    _INF_DIR.mkdir(parents=False, exist_ok=True)
    
    # Source file
    src = _SRC_DIR / "Corp_NZHoR_V2.csv"  # minified: "Corp_NZHoR_V2_minified.csv"
    
    # Inferred preprocessing files / intermediate steps
    inf_basic_cleaned = _INF_DIR / "basic_cleaned"
    inf_basic_cleaned.mkdir(parents=False, exist_ok=True)
    inf_terms = _INF_DIR / "terms.csv"
    inf_terms_marked = _INF_DIR / "terms_marked.csv"
    inf_prep = _INF_DIR / "preprocessed.csv"
    
    # Inferred target datasets
    inf_Q = _INF_DIR / "gpt_generate.csv"
    inf_QA_S = _INF_DIR / "gpt_finetune.txt"
    inf_QA = _INF_DIR / "clf_qa_pairs.csv"


# === Dataset ===

class DataGetter(VerbosePrinter):
    """Simple call for reading a csv file into a pd.DataFrame
    
    The main benefit is that it includes the functionality of VerbosePrinter.
    """
    
    def ds_from_csv(self, data_path: Path, column_names: list = None) -> pd.DataFrame:
        self.print("Get dataset...")
        self.print("from", data_path, indent=1)
        self.print()
        dataset = pd.read_csv(data_path, index_col=0, usecols=column_names)
        return dataset


# === Preprocessing ===

class Cleaner(VerbosePrinter):
    """Class for cleaning the dataset"""
    
    def __init__(self, dataset: pd.DataFrame, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = self._safe_return_dataset(dataset)
    
    @staticmethod
    def _safe_return_dataset(ds) -> pd.DataFrame:
        """Assert that certain columns exist, are correctly ordered and no more columns exist.
        
        :param ds: dataset to `safely` return
        """
        
        assert isinstance(ds, pd.DataFrame), "Invalid data type"
        columns = ["speechnumber", "date", "party", "text"]
        assert set(ds.columns) == set(columns), f"Invalid columns provided. Must have {columns}"
        
        return ds.loc[:, columns]
    
    def _remove_nan_entries(self):
        """Removes all rows that contain NaN in `subset` column."""
        
        n_before = len(self._data)
        self._data.dropna(subset=["text"], inplace=True)
        n_after = len(self._data)
        
        self.print(f"Removed {n_before - n_after} NaN text entries", indent=1)
        return self
    
    def clean(self, from_static=False, from_basic=False):
        """Calls BasicCleaner and TermRepetitionCleaner, or uses static file to overwrite.
        
        :param from_static: Skip cleaning process and use static file instead.
        :param from_basic: Skip `BasicCleaner.clean_data()` and use static file instead.
        """
        
        self.print("Cleaning Data...")
        if not from_static:
            # Basic preprocessing
            cleaner = BasicCleaner(self._data, **self._info)
            data = cleaner.clean_data(from_basic=from_basic)
            # Remove high frequent terms
            cleaner = TermRepetitionCleaner(data, **self._info)
            data = cleaner.clean_data()
        else:
            self.print("Using static preprocessed.", indent=1)
            source_file = FileNames().inf_prep
            data = DataGetter(**self._info).ds_from_csv(source_file)
        return self._safe_return_dataset(data)
    

class BasicCleaner(Cleaner):
    """Class for simple preprocessing of data"""
    
    def __init__(self, dataset: pd.DataFrame, *args, **kwargs):
        super().__init__(dataset, *args, **kwargs)
    
    # --- MAIN ---
    
    def clean_data(self, from_basic=False):
        """Simple clean up, namely:
        
        1. Remove NaN entries.
        2. Remove non-english sentences.
        3. Remove notes / commenting parentheses.
        4. Clean fragments from text removal. (Mainly multiple dots and white spaces.)
        
        :param from_basic: Skip filter process and use static file instead.
        """
        self.print("Basic Preprocessing...")
        self.print("Handling", len(self._data), "rows.", indent=1)
        self._fill_nan_entries()
        self._filter_text(from_basic=from_basic)
        self._remove_nan_entries()
        return self._data
    
    # --- FUNCTIONS ---
    
    def _fill_nan_entries(self):
        self._data["text"].fillna("", inplace=True)
        return self
    
    def _filter_text(self, from_basic=False):
        self.print("Filtering text entries", indent=1)
        store_dir = FileNames().inf_basic_cleaned
        
        spacy_filter = self.TextFilter(store_dir=store_dir, **self._info)
        if not from_basic:
            filtered_text = spacy_filter.transform_in_chunks(self._data["text"])
        else:
            filtered_text = spacy_filter.recombine_chunks()
        
        self._data.loc[:, "text"] = filtered_text
        return self
    
    # --- SUB-CLASSES ---

    class TextFilter(VerbosePrinter):
        def __init__(self, store_dir: Path, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self._store_dir = store_dir
            self.print("Store directory:", self._store_dir, indent=1)
        
        # --- Accessors ---

        def transform_in_chunks(self, texts: pd.Series, chunk_size=50_000):
            """Chunk wise use `self.transform()` function to filter text data.
            
            :param texts: Series of texts to transform
            :param chunk_size: Determines size of chunks
            """
            
            self.print("Chunk wise filtering...")
    
            nlp = self._create_nlp()
            from_ = 0
            while from_ < len(texts):
                to_ = min(from_ + chunk_size, len(texts) + 1)
        
                self.print("Transform and store from", from_, "to", to_, indent=1)
                chunk = texts.iloc[from_:to_]
                chunk_cleaned = self.transform(chunk, nlp=nlp)
                path = self._store_dir / f"chunk_{from_:06d}.csv"
                chunk_cleaned.to_csv(path)
        
                from_ = to_
    
            return self.recombine_chunks()
        
        def transform(self, texts: pd.Series, nlp=None):
            """Filter the text. Keep english sentences only and perform some RegEx cleanup.
            
            :param texts: Series of texts to transform
            :param nlp: spaCy nlp
            """
            
            if nlp is None:
                nlp = self._create_nlp()
            
            # Apply nlp on texts, i.e. sentencize and detect language
            docs = list(nlp.pipe(texts, disable=["tagger", "parser", "ner"]))
            # Filter for english sentences and clean them up
            cleaned = pd.Series(docs).progress_apply(self._filter_document)  # noqa
            cleaned.name = "basic_cleaned"
            cleaned.index = texts.index
            
            return cleaned
        
        def recombine_chunks(self):
            """Combine chunks to one pd.Series"""
            
            self.print("Chunk wise reading stored text...", indent=1)
            
            chunks = []
            for path in sorted(self._store_dir.glob("*.csv")):
                chunk = pd.read_csv(path, index_col=0).iloc[:, 0]
                chunks.append(chunk)
            
            return pd.concat(chunks).sort_index()
        
        # --- Wrangling ---
        
        @staticmethod
        def _create_nlp():
            """Create spaCy nlp."""
            
            nlp = spacy.load("en_core_web_sm")
            
            # Add sentencizer
            sentencizer = nlp.create_pipe("sentencizer")
            nlp.add_pipe(sentencizer)
            # Add language detector
            lang_detector = LanguageDetector()
            nlp.add_pipe(lang_detector, name="language_detector", last=True)
            
            return nlp
        
        def _filter_document(self, doc: Doc) -> str:
            """Filter each spaCy document and return the filtered string.
            
            - keep english sentences only
            - clean sentences
            - clean whole output string
            
            :returns: cleaned string or np.nan if empty
            """
            
            filt_sentences = []
            for sent in doc.sents:
                lang = sent._.language  # noqa
                if lang["language"] == "en" and lang["score"] > .5:
                    sent_text = self._clean_sentence(sent.text)
                    filt_sentences.append(sent_text)
            
            # Combine list of sentences to string
            filt_text = " ".join(filt_sentences)
            
            # Clean
            filt_text = re.sub(r"\[.*\]|\(.*\)", "", filt_text)  # complete brackets
            filt_text = re.sub(r"[^.,?!;]+\]", "", filt_text)  # incomplete right bracket (until previous punctuation)
            filt_text = re.sub(r"\[[^.,?!;]]", "", filt_text)  # incomplete left bracket (before next punctuation)
            filt_text = re.sub(r"\s+", " ", filt_text)  # multi-spaces
            
            # Make NaN if empty
            if filt_text == "":
                filt_text = np.nan
            
            return filt_text
        
        @staticmethod
        def _clean_sentence(text: str) -> str:
            """Clean each sentence with RegEx operations.
            
            - remove parentheses
            - remove multi-occurrences of some special characters
            
            :param text: input string
            :returns: cleaned sentence
            """
            
            # Dash handling
            text = re.sub(r"—", "-", text)  # replace long dash with normal dash
            text = re.sub(r"-{3,}", "--", text)  # max. two dashed | otw. parentheses rule bad (see below)
            
            # Remove parentheses
            punctuation = r"\.\!\?"
            text = re.sub(r"-[^-" + punctuation + r"]*-", ", ", text)  # -...-
            text = re.sub(r"\[[^\[\]" + punctuation + r"]*\]", "", text)  # noqa # [...]
            text = re.sub(r"\([^\(\)" + punctuation + r"]{2,}\)", "", text)  # noqa # (...)
            # text = re.sub(r"\[.*|.*\]", " ", text)  # unfinished bracket
            
            # Replace remaining `-` signs with dot
            text = re.sub(r"-+$", ".", text)  # end of line
            text = re.sub(r"-+[^\w\s\d]", ". ", text)  # protect junctions
            
            # Remove square brackets (before / after sentence)
            text = text.strip().strip('[').strip(']').strip()
            
            # Remove multi-occurrences, e.g. "The   white   fox" -> "The white fox"
            text = re.sub(r"\s{2,}", " ", text)  # whitespaces
            text = re.sub(r"[\s]*\.+", ".", text)  # dots
            text = re.sub(r"[\s]*,+", ",", text)  # commas
            text = re.sub(r",\.", ",", text)  # comma-dot
            
            # Assert that text does not start with punctuation
            text = re.sub(r"^[.,!?;\s]+", "", text)
            
            return text


class TermRepetitionCleaner(Cleaner):
    """Class for removing repetitive terms that appear to being formal speech"""
    
    def __init__(self, dataset: pd.DataFrame, *args, **kwargs):
        super().__init__(dataset, *args, **kwargs)
        self._terms = None
        self._puncts = None
    
    # --- MAIN ---
    
    def clean_data(self):
        """Remove often occurring terms that represent formal speech.

        1. We analysed the term repetitions.
        2. Mark the first 1000 terms if they appear to us as ``formal``.
        3. Remove those terms if both parties used it more or less same often.
        """
        
        self.print("Term Repetition Cleaning...")
        
        # Clean punctuation
        def clean_puncts(lst):
            new_lst = []
            for e in lst:
                e = re.sub(r"\s", "", e)
                e = re.sub(r",{2,}", ",", e)
                e = re.sub(r"\.{2,}", ".", e)
                e = re.sub(r"^,[.]+", ".", e)
                e = re.sub(r"^\.[,.;:?!]+", ".", e)
                e = re.sub(r"^;[,.;:?!]+", ";", e)
                e = re.sub(r"^:[,.;:?!]+", ":", e)
                e = re.sub(r"^\?[,.;:?!]+", "?", e)
                e = re.sub(r"^![,.;:?!]+", "!", e)
                new_lst.append(e + " ")
            return new_lst
        
        # Assert punctuation and white space at the end
        self._data["text"] = self._data["text"] + ". "
        
        # Split text by punctuation // put in separate series of lists
        self.print("Split text by punctuation", indent=1)
        regex = r"([.,!?;:][.,!?;:\s]*\s)"
        # ignore last list element, since it's an empty string
        splits = self._data["text"].str.split(regex).rename("terms").apply(lambda lst: lst[:-1])
        self._terms = splits.apply(lambda lst: [e.strip() for e in lst[::2]]).rename("terms")
        self._puncts = splits.apply(lambda lst: lst[1::2]).apply(clean_puncts).rename("punctuations")
        
        mask = self._terms.apply(len) != self._puncts.apply(len)
        assert len(mask[mask]) == 0, "# terms and # puncts are not equal for all rows."
        
        # Analyze and filter all terms
        # self._analyze_term_repetition()  # ATTENTION: Uncomment if want to analyze
        self._delete_repetitive_terms()

        # Recombine text and punctuation
        elementwise_add = [[str(term) + str(punct) for term, punct in zip(t_row, p_row)]
                           for t_row, p_row in zip(self._terms, self._puncts)]
        text = ["".join(text).strip() for text in elementwise_add]
        
        # Empty strings --> NaN
        text = [t if t != "" else np.nan for t in text]
        
        # Overwrite text with clean text
        self._data["text"] = text
        
        # Drop Nans
        self._remove_nan_entries()
        
        # Store data
        store_file = FileNames().inf_prep
        self._data.to_csv(store_file)
        
        return self._data
    
    def _analyze_term_repetition(self):
        """Analyse term repetitions for both parties individually and store as *.csv file for analysis by hand"""
        
        store_path = FileNames().inf_terms
        self.print("Analysing repetition of terms.", indent=1)
        self.print("Output file:", store_path, indent=2)
        
        # Separate National and Labour terms / Value counts
        is_nat = self._data["party"] == "National"
        is_lab = self._data["party"] == "Labour"
        nat_terms = self._terms[is_nat].explode().str.lower().value_counts(normalize=True).rename("nat")
        lab_terms = self._terms[is_lab].explode().str.lower().value_counts(normalize=True).rename("lab")
        
        # Calculate min-max-difference of value count
        terms = pd.concat([nat_terms, lab_terms], join="inner", axis=1)
        terms["diff"] = (terms.max(1) - terms.min(1)) / terms.max(1)
        terms.to_csv(store_path)
        
        return self
    
    def _delete_repetitive_terms(self):
        """Remove repetitive terms. Criterion is, firstly, the hand selection and,
        secondly, the ((max-min)/max) < 0.5 for the two parties.
        """
        
        self.print("Delete repetitive terms", indent=1)
        
        source_path = FileNames().inf_terms_marked
        terms_marked = pd.read_csv(source_path)
        terms_to_del = terms_marked.loc[terms_marked["diff"] < 0.5, "term"].dropna().tolist()
        
        # Get and apply filter mask
        for row in self._terms.index:
            mask = []
            for i, string in enumerate(self._terms.loc[row]):
                if string.lower() in terms_to_del:
                    mask.append(False)
                else:
                    mask.append(True)
            # Filter
            self._terms.loc[row] = np.array(self._terms.loc[row])[mask]
            self._puncts.loc[row] = np.array(self._puncts.loc[row])[mask]
        
        return self


# === Split up Dataset ===

class DataSplitter(VerbosePrinter):
    """Class for splitting up dataset for later usage in GPT-2."""
    
    def __init__(self, dataset, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._data = self._pre_filter_data(dataset)
        self._Q_index = None  # index of questions w/o answer
        self._QA_index = None  # index of questions for Q/A pairs
        self._S_index = None  # index of remaining statements
    
    def split_data(self):
        """Split up data in a question, question-answer and statement dataset."""
        
        self.print("Split dataset in sub-datasets...")
        self._find_questions()._find_qa_pairs()._find_statements()
        self._print_summary()
        self._shuffle_data_indices()
        return self
    
    def _print_summary(self):
        self.print("Summary:", indent=1)
        self.print(len(self._QA_index), "question-answer pairs.", indent=2)
        self.print(len(self._Q_index), "sole questions.", indent=2)
        self.print(len(self._S_index), "sole statements.", indent=2)
        return

    # --- Helper ---

    def _return_party_balanced_index(self, legal_index: pd.Index) -> pd.Index:
        """
        :param legal_index: initial index to be balanced across National and Labour
        :returns: a subset of the given indices s.t. both parties are balanced
        """
        
        # Select party answers
        party = self._data.loc[legal_index, "party"]
        nat = party[party == "National"]
        lab = party[party == "Labour"]
        # Balance party answers
        n_from_party = min(len(nat), len(lab))
        nat_index = nat.sample(n=n_from_party, random_state=69).index
        lab_index = lab.sample(n=n_from_party, random_state=67).index
        
        # Recombine
        balanced_index = nat_index.union(lab_index)
        
        return balanced_index
    
    def _shuffle_data_indices(self):
        """Pseudo-randomly shuffle dataset indices."""
        
        self.print("Shuffle datasets pseudo randomly.", indent=1)
        np.random.seed(123)
        
        self._Q_index = np.random.permutation(self._Q_index)
        self._QA_index = np.random.permutation(self._QA_index)
        self._S_index = np.random.permutation(self._S_index)
        
        return self
    
    # --- Filter functions ---
    
    @staticmethod
    def _pre_filter_data(dataset: pd.DataFrame):
        """Prefilter dataset. Each text entry must contain > 8 characters."""
        
        mask = dataset["text"].apply(lambda x: len(x) > 8)
        return dataset[mask]
    
    def _find_questions(self):
        """Find all questions from the dataset and store indices.
        
        Criteria:
        - must contain question mark
        - length of text is in (32, 128)
        - text has only one sentence
        - text has no personal aspect >> probably out of context to answer that question
        """
        
        self.print("Search potential questions...", indent=1)
        text = self._data["text"]
        
        is_question = text.str.contains("?", regex=False)
        text = text[is_question]
        
        has_good_length = text.apply(lambda x: 32 < len(x) < 128)
        text = text[has_good_length]
        
        is_one_sentence = text.str.split(r"[.;:!?]+").apply(lambda x: len(x) == 2)
        text = text[is_one_sentence]
        
        array_personal = [rf"{word}" for word in ["they", "their", "his", "her", "him", "he", "she"]]
        regex_personal = r"(^|\s)(" + r"|".join(array_personal) + r")(\s|$|\')"
        is_personal = text.str.count(regex_personal, flags=re.IGNORECASE) > 0
        text = text[~is_personal]
        
        self._Q_index = text.index
        return self
    
    def _find_qa_pairs(self):
        """Find answers to the questions, whereby the answer must origin from either a National or Labour partisan."""
        
        self.print("Search potential answers...", indent=1)
        
        # Find answer indices
        Q_index = self._Q_index
        A_index = self._data.index.intersection(Q_index + 1)  # A_index must exist in data
        A_index = A_index[~A_index.isin(Q_index)]  # A_index must be distinct from Q_index
        # Speechnumber must be consecutive
        Q_speech = self._data.loc[A_index - 1, "speechnumber"] + 1
        Q_speech.index += 1
        A_speech = self._data.loc[A_index, "speechnumber"]
        A_index = (Q_speech == A_speech).index
        
        # Get party-balanced indices of answers
        A_index = self._return_party_balanced_index(A_index)
        QA_index = A_index - 1
        
        # Set question-answer index as Q_index
        self._QA_index = QA_index
        
        # Q_index must be distinct from QA_index
        Q_index = Q_index[~Q_index.isin(QA_index)]
        self._Q_index = Q_index
        
        return self
    
    def _find_statements(self):
        """All text that is not recognised as a valid question or answer will be used as statement,
        as long as it origins from either a National or Labour partisan."""
        
        self.print("Assign remaining statements...", indent=1)
        
        # Remove indices that are in use for Q or QA dataset
        Q_QA_index = self._Q_index.union(self._QA_index).union(self._QA_index + 1)
        S_index = ~self._data.index.isin(Q_QA_index)

        # Get party-balanced indices of statements
        S_index = self._return_party_balanced_index(S_index)
        
        # Set statement index as S_index
        self._S_index = S_index
        
        return self
    
    # --- Storing ---
    
    def store_datasets(self):
        """Store datasets for later usage in GPT-2 or the classifier.
        
        - ds_Q.csv >> for GPT-2 answer generation
        - ds_QA_S.txt >> for GPT-2 fine tuning
        - ds_QA.csv >> for classification
        """
        
        self.print("Prepare and store datasets...", indent=1)
        
        # Handle questions
        Q_data = self._data.loc[self._Q_index]
        Q_text_nat = "[Question]: " + Q_data["text"] + "\n[Answer, National]:"
        Q_text_lab = "[Question]: " + Q_data["text"] + "\n[Answer, Labour]:"
        Q_text = pd.concat([Q_text_nat, Q_text_lab], axis=1).stack()
        
        # Handle question-answer pairs
        QA_q_data = self._data.loc[self._QA_index]
        QA_a_data = self._data.loc[self._QA_index + 1]
        QA_a_data.index -= 1
        QA_data = pd.DataFrame({
            "question": QA_q_data["text"],
            "party": QA_a_data["party"],
            "answer": QA_a_data["text"],
        })
        QA_text = "[Question]: " + QA_data["question"] \
            + "\n[Answer, " + QA_data["party"] + "]: " + QA_data["answer"]
        
        # Handle statements
        S_data = self._data.loc[self._S_index]
        S_text = "[Statement, " + S_data["party"] + "]: " + S_data["text"]
        
        # Combine QA and S
        QA_S_text = QA_text.append(S_text).sample(frac=1, random_state=420)
        QA_S_text += "<|endoftext|>\n\n"
        
        # Define file paths
        f_names = FileNames()
        Q_store = f_names.inf_Q
        QA_S_store = f_names.inf_QA_S
        QA_store = f_names.inf_QA
        # Store
        Q_text.to_csv(Q_store, index=False, header=None, sep=";")
        with open(QA_S_store, "w") as file:
            for row in QA_S_text:
                file.write(row)
        QA_data.to_csv(QA_store, index=False, sep=";")
        
        self.print("Question dataset          >>", Q_store, indent=2)
        self.print("Q&A and Statement dataset >>", QA_S_store, indent=2)
        self.print("Question-answer dataset   >>", QA_store, indent=2)
        
        return


# === MAIN ===

def main(verbose=0, from_static=False, from_basic=False):
    # Get data
    f_names = FileNames()
    column_names = ["Unnamed: 0", "speechnumber", "date", "party", "text"]
    dataset = DataGetter(verbose=verbose).ds_from_csv(f_names.src, column_names).reset_index(drop=True)
    
    # Preprocessing
    dataset = Cleaner(dataset, verbose=verbose).clean(from_static=from_static, from_basic=from_basic)
    
    # Split data
    DataSplitter(dataset, verbose=verbose).split_data().store_datasets()
    
    return


if __name__ == '__main__':
    main(3, True, True)


__author__ = "Timo Streule"
__credits__ = ["Marc Styger"]
__email__ = "tstreule@student.ethz.ch"

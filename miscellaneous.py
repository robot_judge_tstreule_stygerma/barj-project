class VerbosePrinter:
    """Pretty printer function based on verbosity level and desired indent."""
    
    def __init__(self, verbose: int = 0, verbose_indent: int = 0, inherited: bool = False):
        """
        :param verbose: verbosity of print
        :param verbose_indent: standard indent for print
        :param inherited: if True will decrease `verbose` and increase `verbose_indent` by 1
        """
        
        # Decrease verbose and increase indent such that you can simply
        # forward `self._V_info` params for the next subclass
        self._verbose = int(verbose)
        self._verbose_indent = int(verbose_indent)
        if inherited:
            self._verbose -= 1
            self._verbose_indent += 1
        self._info = dict(verbose=self._verbose, verbose_indent=self._verbose_indent, inherited=True)
    
    def print(self, *args, indent=0, **kwargs):
        """Prints when verbosity. Use `indent` > 0 to indent the printing string."""
        
        if self._verbose >= 0:
            indent = int(indent)
            # Prevent indent when empty print statement
            if len(args) == 0:
                pass
            # Add indentation
            elif self._verbose_indent + indent > 0:
                indentation = "    " * (self._verbose_indent + indent - 1)
                if indent == 0:
                    indentation += ">>  "
                else:
                    indentation += "..  "
                # Remove trailing white space
                args = indentation[:-1], *args
            # Print statement
            print(*args, **kwargs)
        
        return

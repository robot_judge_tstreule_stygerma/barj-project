import re
from pathlib import Path

import numpy as np
import pandas as pd
import torch
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, precision_recall_fscore_support

from transformers import (
    DistilBertTokenizerFast, PreTrainedTokenizer,
    DistilBertForSequenceClassification, Trainer, TrainingArguments,
)


# === DATA WRANGLING === #

def _read_data(directory: str):
    # Iterate all *.csv files and add to `data`
    questions = []
    labour = []
    national = []
    path_list = Path(directory).glob('*.csv')
    for path in path_list:
        tmp_data = pd.read_csv(path)
        questions.extend(tmp_data["Question"].tolist())
        labour.extend(tmp_data["Labour-answer"].tolist())
        national.extend(tmp_data["National-answer"].tolist())
    
    # Clean data
    def clean(x):
        output = re.sub("[<][^\>]*[>]", "", x)  # noqa # Delete <...> pattern
        output = re.sub("\.[^\.]*$", ".", output)  # noqa # Delete unfinished last sentence
        return output
    questions = pd.Series(questions).apply(clean)  # noqa
    labour = pd.Series(labour).apply(clean)  # noqa
    national = pd.Series(national).apply(clean)  # noqa
    
    # Prepare data
    labour_data = pd.DataFrame(dict(question=questions, answer=labour, label=0))
    national_data = pd.DataFrame(dict(question=questions, answer=national, label=1))
    data = pd.concat([labour_data, national_data], ignore_index=True)
    
    # Drop too long texts
    has_valid_length = data.answer.apply(lambda x: len(str(x))) < 1023
    data = data[has_valid_length]
    
    return data.answer.tolist(), data.label.tolist()


class NZDataset(torch.utils.data.Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item['labels'] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


def get_datasets(tokenizer: PreTrainedTokenizer,
                 train_test_ratio=.8, train_eval_ratio=.8,
                 path_to_dir="inferred/02_gpt2/generated_answers"):
    # Split data
    texts, labels = _read_data(path_to_dir)
    print()
    train_texts, test_texts, train_labels, test_labels = \
        train_test_split(texts, labels, train_size=train_test_ratio)
    train_texts, eval_texts, train_labels, eval_labels = \
        train_test_split(train_texts, train_labels, train_size=train_eval_ratio)

    # Get encodings
    train_encodings = tokenizer(train_texts, truncation=True, padding=True)
    eval_encodings = tokenizer(eval_texts, truncation=True, padding=True)
    test_encodings = tokenizer(test_texts, truncation=True, padding=True)
    
    # Create tensorflow dataset
    train_ds = NZDataset(train_encodings, train_labels)
    eval_ds = NZDataset(eval_encodings, eval_labels)
    test_ds = NZDataset(test_encodings, test_labels)
    
    return train_ds, eval_ds, test_ds


# === Language Model === #

def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    precision, recall, f1, _ = precision_recall_fscore_support(labels, preds, average='binary')
    acc = accuracy_score(labels, preds)
    return {
        'accuracy': acc,
        'f1': f1,
        'precision': precision,
        'recall': recall
    }


# === MAIN === #

def main():
    model = DistilBertForSequenceClassification.from_pretrained("distilbert-base-uncased", do_lower_case=True)
    tokenizer = DistilBertTokenizerFast.from_pretrained("distilbert-base-uncased")
    train_ds, eval_ds, test_ds = get_datasets(tokenizer)
    
    inf_path = Path("./inferred/03_discriminator/model_transformers")

    training_args = TrainingArguments(
        output_dir=inf_path / "results",  # output directory
        num_train_epochs=3,  # total number of training epochs
        per_device_train_batch_size=32,  # batch size per device during training
        per_device_eval_batch_size=64,  # batch size for evaluation
        warmup_steps=500,  # number of warmup steps for learning rate scheduler
        weight_decay=0.01,  # strength of weight decay
        logging_dir=inf_path / "logs",  # directory for storing logs
        logging_steps=10,
        # evaluation_strategy="epoch",
    )

    trainer = Trainer(
        model=model,  # the instantiated 🤗 Transformers model to be trained
        args=training_args,  # training arguments, defined above
        train_dataset=train_ds,  # training dataset
        eval_dataset=eval_ds,  # evaluation dataset
        compute_metrics=compute_metrics,
    )

    trainer.train()
    trainer.save_model(inf_path)
    
    pred = trainer.predict(test_ds)

    print("\n" + "=" * 30 + "\n")
    print("true labels:", list(pred.label_ids))
    print("pred labels:", np.argmax(pred.predictions, axis=1).tolist())
    print("metrics:")
    for key in pred.metrics:
        print(f"  {key}: {round(pred.metrics[key], 4)}")
    print("\n" + "=" * 30 + "\n")
    print("train labels:", list(train_ds.labels))
    print("eval labels: ", list(eval_ds.labels))
    print("eval labels: ", list(eval_ds.labels))
    
    return


if __name__ == '__main__':
    main()

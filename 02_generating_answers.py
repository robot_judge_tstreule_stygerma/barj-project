import pandas as pd
import numpy as np
import gpt_2_simple as gpt2
import sys

assert len(sys.argv) == 3, "Input two integers with first < second to specify the range over which the finetuning operations works"
from_ = int(sys.argv[1])
to = int(sys.argv[2])
print("Running from", from_, "to", to)


#Get the latest checkpoint of the fine_tuned GPT-2 model
RUN_NAME = 'NZ_Parliament_QA'
sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess, run_name=RUN_NAME)


#Include the data used for generation. Each question appears twice, each time with a different party tag
data = pd.read_csv("inferred/01_preprocessing/gpt_generate.csv", header=None, sep=";").iloc[:, 0]
data_new = pd.DataFrame(index=data.index[:int(len(data.index)/2)], columns=["National", "Labour"]) 
data_new.iloc[:,0] = np.array(data.iloc[0::2])
data_new.iloc[:,1] = np.array(data.iloc[1::2])
data_new = data_new.iloc[from_:to]

#Provide each question to the finetuned gpt-model which generates an answer. Each row that is an input to this function 
#consists of the same question twice but each time with a different party tag for the required answer.
def generate_answers(row):
    count=0
    question = row.iloc[0]
    for i in row:
        answer = gpt2.generate(sess,
            temperature=0.7,
            prefix=i,
            nsamples=1,
            batch_size=1,
            length=100,
            run_name="NZ_Parliament_QA",
            return_as_list=True)[0]
        if count == 0:
            answer_national = answer
        elif count == 1:
            answer_labour = answer
        count += 1
    return pd.Series([question, answer_labour, answer_national], index=["Question", "Labour-answer", "National-answer"])

generated_data = data_new.apply(generate_answers, axis=1)
print("First generated output", generated_data.iloc[0])


#Remove the tags and possible statements beyond the required answer
def clean_generated_data(row):
    row.iloc[0] = row.iloc[0].split(': ')[1].split('\n')[0]
    row.iloc[1] = row.iloc[1].split('[')[2].split(': ')[1]
    row.iloc[2] = row.iloc[2].split('[')[2].split(': ')[1]
    return row

cleaned_generated_data = generated_data.apply(clean_generated_data, axis=1)


#Store the results such that each row consists of a question, an answer and the party tag associated to the answer.
national_tag = pd.Series(index=pd.Index(range(len(cleaned_generated_data.index)*2)))
labour_tag = pd.Series(index=data.index[:int(len(data.index)/2)])
national_tag.iloc[:] = "National"
labour_tag.iloc[:] = "Labour"

output = pd.DataFrame(index=pd.Index(range(len(cleaned_generated_data.index)*2)), columns=["question", "party", "answer"], dtype=str)

output.iloc[0::2, 0] = np.array(cleaned_generated_data.iloc[:, 0])
output.iloc[1::2, 0] = np.array(cleaned_generated_data.iloc[:, 0])
output.iloc[0::2, 2] = np.array(cleaned_generated_data.iloc[:, 1])
output.iloc[1::2, 2] = np.array(cleaned_generated_data.iloc[:, 2])  
output.iloc[0::2, 1] = labour_tag
output.iloc[1::2, 1] = national_tag

output.to_csv('inferred/02_gpt2/generated_answers_{}_{}.csv'.format(from_, to), index=False, sep=(';'))
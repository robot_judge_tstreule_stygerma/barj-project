from pathlib import Path
import re

# For data handling
import scipy.sparse
import numpy as np
import pandas as pd
import datasets
from datasets import DatasetDict

# For language processing and word embeddings
import spacy
import gensim
import gensim.downloader as gensim_api
from tensorflow.keras import preprocessing as keras_processing

# For preprocessing
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import SelectFromModel
from sklearn.ensemble import RandomForestClassifier

# For classification
from sklearn.svm import LinearSVC, SVC
from sklearn.model_selection import GridSearchCV

# For (custom) pipeline components
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, TransformerMixin

# Metrics
from sklearn.metrics import classification_report

# Miscellaneous
from miscellaneous import VerbosePrinter


# === Data Paths === #

class FileNames:
    _SRC_DIR = Path("inferred/02_gpt2")
    _INF_DIR = Path("inferred/03_discriminator")
    _INF_DIR_SRC = _INF_DIR / "preprocessed"
    _INF_DIR_SRC.mkdir(parents=True, exist_ok=True)
    
    src = _SRC_DIR / "generated_answers"
    src_X_train = _INF_DIR_SRC / "X_train.npz"  # scipy.sparse
    src_y_train = _INF_DIR_SRC / "y_train.csv"
    src_X_test = _INF_DIR_SRC / "X_test.npz"  # scipy.sparse
    src_y_test = _INF_DIR_SRC / "y_test.csv"
    inf = _INF_DIR


# === DATASET PREPARATION === #

def get_dataset(data_path: Path, train_size=.8, verbose=0) -> DatasetDict:
    """Read out data from `data_path`, and prepare textual data for pre-processing.
    
    :param data_path: path to folder, containing *.csv files (columns: `Question`, `National-answer`, `Labour-answer`)
    :param train_size: ratio of train-test-split
    :param verbose: verbosity
    :return: prepared dataset with train/eval/test split
    """
    if verbose > 0:
        print("Get dataset...")
    
    # Read data
    data_files = [path for path in Path(data_path).glob('*.csv')]
    dataset = datasets.load_dataset("csv", data_files=data_files, split="train")
    dataset.rename_column_("Question", "question")
    
    # Create labels
    national_ds = dataset.map(lambda data: dict(answer=data["National-answer"], label="National", label_enc=0),
                              remove_columns=["National-answer", "Labour-answer"])
    labour_ds = dataset.map(lambda data: dict(answer=data["Labour-answer"], label="Labour", label_enc=1),
                            remove_columns=["National-answer", "Labour-answer"])
    dataset = datasets.concatenate_datasets([national_ds, labour_ds])
    
    # Rough cleanup of the text
    def clean_answers(data: dict) -> dict:
        answer = str(data["answer"])
        answer = answer[:512]  # truncate to limit length
        answer = re.sub(r"[<][^\>]*[>]", "", answer)  # find <...> pattern
        # answer = re.sub(r"\.[^\.]*$", ".", answer)  # find unfinished last sentences
        return dict(answer=answer)
    
    dataset = dataset.map(clean_answers)
    num_rows_before = len(dataset)
    dataset = dataset.filter(lambda data: 16 < len(data["answer"]))  # too small
    num_rows_after = len(dataset)
    
    # Split into train and test dataset
    dataset = dataset.train_test_split(train_size=train_size, seed=420)
    
    if verbose > 0:
        print("Dataset with columns", dataset["train"].column_names)
        print(f">>  {num_rows_before-num_rows_after} of {num_rows_before} rows were filtered out (text length)")
        for split in dataset:
            y = pd.Series(dataset[split]["label"])
            n_national = len(y.loc[y == "National"])
            n_labour = len(y.loc[y == "Labour"])
            to_print = {"shape": dataset[split].shape,
                        "Nat-Lab ratio": round(n_national/n_labour, 3)}
            print(f">>  {split}:", to_print)
        print("")
    
    return dataset


# === TF-IDF / BAG OF WORDS === #

class SpacyCleaner(BaseEstimator, TransformerMixin):
    """Use spaCy to lemmatize text and filter out stop words."""
    
    def __init__(self):
        super().__init__()
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.transform(X)
    
    def transform(self, X):
        X = pd.Series(X).apply(lambda x: str(x).lower().strip())  # noqa
        nlp = spacy.load("en")
        docs = list(nlp.pipe(X, disable=["tagger", "parser", "ner"]))
        docs = pd.Series(docs)
        X = pd.Series(docs).apply(self._spacy_process)  # noqa
        return X.tolist()
    
    @staticmethod
    def _spacy_process(doc) -> str:
        # answer = str(answer).lower().strip()
        # doc = nlp(answer)
        
        valid_tokens = []
        for token in doc:
            is_valid = True
            is_valid = is_valid and not token.is_stop  # ignore stop words
            # is_valid = is_valid and token.is_alpha  # consist of alphabetic characters
            # TODO: Think about using a placeholder for alphabetic characters instead
            if is_valid:
                valid_tokens.append(token.lemma_)  # append lemmatized word to list
        
        # Revert list to string of valid, lemmatized words
        lemmas = " ".join(valid_tokens)
        return lemmas


class Word2Vec(BaseEstimator, TransformerMixin):
    """Vectorization of text according to
    https://towardsdatascience.com/text-classification-with-nlp-tf-idf-vs-word2vec-vs-bert-41ff868d1794
    """
    
    def __init__(self, random_state=1):
        super().__init__()
        self._random_state = random_state
        self._vec_size = 300
        self._vocabulary = {}
        self._nlp = None
        self._idf = None
        self._embedding = None
    
    def fit(self, X, y=None):
        # Create list of lists of 1-grams
        # lst_corpus = pd.Series(X).apply(lambda text: str(text).split())  # noqa
        
        # Create vocabulary
        tokenizer = keras_processing.text.Tokenizer(lower=True, split=" ", oov_token=None)
        tokenizer.fit_on_texts(X)
        self._vocabulary = tokenizer.word_index
        self._vocabulary["NaN"] = 0  # add oov_token
        
        # Get inverse document frequency
        tf_idf = TfidfVectorizer(vocabulary=self._vocabulary)
        tf_idf.fit(X)
        self._idf = tf_idf.idf_
        
        # Create our own Word2Vec
        self._nlp = gensim.models.word2vec.Word2Vec(
            X, size=self._vec_size, window=8, min_count=1, sg=1, iter=30, seed=self._random_state
        )  # self._nlp = gensim_api.load("word2vec-google-news-300")  # pre-trained
        
        # Create word -> word2vec embedding
        self._embedding = np.zeros(shape=(len(self._vocabulary), self._vec_size))
        for word, idx in self._vocabulary.items():
            try:
                self._embedding[idx] = self._nlp.wv[word]  # word vector
            except:  # noqa
                continue
        
        ########
        
        """
        # tokenize text
        tokenizer = kprocessing.text.Tokenizer(lower=True, split=" ",
                                               oov_token="NaN",
                                               filters='!\"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n')
        tokenizer.fit_on_texts(lst_corpus)
        dic_vocabulary = tokenizer.word_index
        print(dic_vocabulary)
        
        # create sequence
        lst_text2seq = tokenizer.texts_to_sequences(lst_corpus)
        print(lst_text2seq)
    
        # padding sequence
        X_train = kprocessing.sequence.pad_sequences(lst_text2seq,
                                                     maxlen=30, padding="post", truncating="post")
        print("shape X_train", X_train.shape)
    
        import seaborn as sns
        import matplotlib.pyplot as plt
        sns.heatmap(X_train == 0, vmin=0, vmax=1, cbar=False)
        plt.show()
        """
        
        return self
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X).transform(X)
    
    def transform(self, X):
        # Get word vectors, weighted by tf-idf
        X_vec = np.zeros(shape=(len(X), self._vec_size))
        for idx, text in enumerate(X):
            vector = np.zeros(shape=(self._vec_size,))
            N = 0
            for word in text.split(" "):
                if word in self._vocabulary:
                    N += 1
                    i = self._vocabulary[word]
                    vector += self._idf[i] * self._embedding[i]
                else:
                    continue
            if N != 0:
                vector /= N
            X_vec[idx] = vector
        return X_vec


class TFIDFWord2Vec(BaseEstimator, TransformerMixin):
    """According to Support Vector Machines and Word2vec for Text Classification with Semantic Features
    by J. Lilleberg, et. al.
    """
    
    def __init__(self, use_tfidf: bool = True, use_word2vec: bool = False,
                 params_tfidf=None, params_word2vec=None):
        super().__init__()
        if params_tfidf is None:
            params_tfidf = {}
        if params_word2vec is None:
            params_word2vec = {}
        self._use_tfidf = use_tfidf
        self._use_word2vec = use_word2vec
        self._TfidfVectorizer = TfidfVectorizer(**params_tfidf)
        self._Word2Vec = Word2Vec(**params_word2vec)
    
    def fit(self, X, y=None):
        if self._use_tfidf:
            self._TfidfVectorizer.fit(X, y)
        if self._use_word2vec:
            self._Word2Vec.fit(X, y)
        return self
    
    def transform(self, X):
        if self._use_tfidf and self._use_word2vec:
            X_tfidf = self._TfidfVectorizer.transform(X)
            X_word2vec = self._Word2Vec.transform(X)
            X = scipy.sparse.hstack([X_tfidf, X_word2vec])
        elif self._use_tfidf:
            X = self._TfidfVectorizer.transform(X)
        elif self._use_word2vec:
            X = self._Word2Vec.transform(X)
        else:
            raise ValueError("Nothing to transform. Set `use_tfidf` and/or `use_word2vec` as True.")
        return X
        # return np.concatenate((X_tfidf, X_word2vec), axis=1)
    
    def fit_transform(self, X, y=None, **fit_params):
        return self.fit(X, y).transform(X)


class TextClassifier:
    def __init__(self, dataset: DatasetDict, use_tfidf: bool, use_word2vec: bool, verbose=0):
        train = dataset["train"]
        test = dataset["test"]
        self.X_train = train["answer"]
        self.X_test = test["answer"]
        self.y_train = np.array(train["label_enc"], dtype=int)
        self.y_test = np.array(test["label_enc"], dtype=int)
        self._pipe = None
        self._use_tfidf = use_tfidf
        self._use_word2vec = use_word2vec
        self._verbose = verbose
    
    def print(self, *args, **kwargs):
        """Print if verbose."""
        if self._verbose > 0:
            print(*args, **kwargs)
        return
    
    def _create_pipeline(self) -> Pipeline:
        param_grid = {
            "C": np.linspace(10, 1e4, 4),
            "kernel": ["poly", "rbf"],
            "degree": [3, 4],
            "tol": np.linspace(1e-6, 1e-3, 4),
            # "class_weight": ["balanced"],
            "random_state": [69],
        }
        # TODO: Delete that hard coded param_grid
        param_grid = {'C': 10.0, 'degree': 3, 'kernel': 'rbf', 'random_state': 69, 'tol': 1e-06}
        pipe = Pipeline([
            ("text_cleaner", SpacyCleaner()),
            ("feature_creation", TFIDFWord2Vec(
                use_tfidf=self._use_tfidf,
                use_word2vec=self._use_word2vec,
                params_tfidf=dict(ngram_range=(1, 4)),
                params_word2vec=dict(),
            )),
            ("scaling", StandardScaler(copy=False, with_mean=False)),
            ("feature_selection", SelectFromModel(RandomForestClassifier(random_state=420))),
            # TODO: replace that hard coded param_grid with GridSearchCV
            ("classifier", SVC(**param_grid)),
            # ("classifier_cv", GridSearchCV(
            #     SVC(), param_grid=param_grid, cv=5,
            #     scoring="accuracy", refit=True, n_jobs=-1
            # )),
        ], verbose=self._verbose)
        return pipe
    
    def do_classification(self):
        self.print("Train classifier...")
        
        self._pipe = self._create_pipeline()
        self._pipe.fit(self.X_train, self.y_train)
        y_test_pred = self._pipe.predict(self.X_test)
        """
        self.print("Best parameter (CV score=%0.3f):" % self._pipe.steps[-1][1].best_score_)
        self.print(self._pipe.steps[-1][1].best_params_)
        self._pipe.steps.pop(-1)  # skip classification
        self.print(">>  # columns w/ feature selection:", self._pipe.transform(self.X_train[:1]).shape[1])
        self._pipe.steps.pop(-1)  # skip feature selection
        self.print(">>  # columns w/o feature selection:", self._pipe.transform(self.X_train[:1]).shape[1])
        """
        return self.y_test, y_test_pred
    
    def get_pipe(self):
        return self._pipe


class BaselineClassifier:
    def predict(self, dataset: DatasetDict, mode="random", random_state=420):
        test_ds = dataset["test"]
        y_true = np.array(test_ds["label_enc"])
        length = len(y_true)
        
        if mode == "random":
            np.random.seed(random_state)
            y_pred = np.random.randint(2, size=length)
        elif mode == "ones":
            y_pred = np.ones(shape=length)
        elif mode == "zeros":
            y_pred = np.zeros(shape=length)
        else:
            raise NotImplementedError(f"mode {mode} does not exist")
        
        return y_true, y_pred


# === MAIN === #

def main(verbose=0):
    # Prepare data
    f_names = FileNames()  # define file names
    dataset = get_dataset(data_path=f_names.src, verbose=verbose)
    
    y_test = pd.Series(dataset["test"]["label_enc"]).value_counts()
    print(y_test)
    
    # Use pipeline
    model = TextClassifier(dataset=dataset, use_tfidf=True, use_word2vec=True, verbose=verbose)
    y_test, y_test_pred = model.do_classification()
    
    # Evaluation
    report = classification_report(y_test, y_test_pred)
    print("Classification Report:")
    print(report)
    print(pd.Series(y_test_pred).value_counts())

    import matplotlib.pyplot as plt
    from sklearn import metrics
    clf = model.get_pipe()
    X_test = model.X_test
    fig, ax = plt.subplots(figsize=(10, 10))
    metrics.plot_roc_curve(clf, X_test, y_test, ax=ax)
    plt.show()

    # Baseline classifier
    """
    mode = ["random", "ones", "zeros"][1]
    y_test, y_test_baseline = BaselineClassifier().predict(dataset, mode=mode)
    print(y_test)
    report = classification_report(y_test, y_test_baseline)
    print(f"Baseline classifier (mode \"{mode}\"):")
    print(report)
    """
    
    return


if __name__ == '__main__':
    main(verbose=2)

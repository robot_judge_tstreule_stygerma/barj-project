# Discussion w/ Elliott

## New Preprocessing

Starting from the initial (whole) dataset:

1. Basic text cleaning

   - parenthesis
   - non-english
   - NaN / empty
   - multiple dots + white spaces
   - <u>remove text with <8 characters</u> → few info content
     - Yes/No answer detection

   

2. Filter QUESTIONS

   1. **One**-sentencer with question mark
   2. Keep questions **short/long** enough (>32 and <128 characters)
   3. <u>Remove **personal** questions</u>
      - ["i ", "they ", "their ", "my ", "we ", "our ", "his ", "her ", "him ", "he ", "she "]
        - **Don't drop I, my, we, our**
        - (for conclusion / future work) could do co-reference resolution on the pronouns in the questions
      - → quite radically
        from 168'282
        to 19'104
   4. → Find Q/A-pairs
   5. Randomly **sample** for 5'000 questions
   6. <u>The rest will be for non-question dataset</u>

   

3. Filter NON-QUESTIONS

   1. Select only "National" or "Labour"
   2. <u>Handle often occurring terms:</u>
      1. For both, National and Labour, separately:
         1. Split text into fragments (separated by punctuation)
         2. Value counts
         3. Decide **by hand / intuitively** which ones are "bad"
      2. Delete all fragments, which appear in both parties and are labelled as "bad"
   3. Basic cleaning
      - NaN / empty
      - multiple dots + white spaces

**Remarks**:

1. Find Questions
2. Find (subsequent) answers to those questions
   - If no answer / not Nat. or Lab. → Q dataset
   - Else → potential Q/A dataset
     - need to balance Nat. and Lab.
     - → sample
     - The rest of the Q/A pairs will be split up:
       - Q: goes to Q dataset
       - A: goes to S (Statement) dataset
3. Mark all rows from Q and Q/A dataset as "in use"
4. All remaining rows will be put into a S (Statement) dataset if party ∈ {Nat., Lab.}

...

Given: Q, Q/A and S dataset
	90% for training | 10% for evaluation

1. Q/A and S dataset → use for finetuning
   1. Q/A dataset: nice to learn the Q/A syntax
      ```[Question]: QUESTION \n [Answer, PARTY]: ANSWER ```
   2. S dataset: nice to learn speech sentiments, ...
      ```[Statement, PARTY]: STATEMENT```
2. Q dataset → use for answer generation
   ```[Question]: QUESTION \n [Answer, PARTY]:  (generate here...)``` 

## New GPT Training

1. GPT fine-tuning
   	- using non-question dataset
   	- with <u>manual early stopping</u>
2. Generate answers
   - using question dataset
   - (very slow, but not improvable as far as we know)

## New Classification

<u>Do TWO classifications:</u>

1. Train + predict a classifier on the original (human made) text.
   → use its performance to compare to the second
2. Train + predict a classifier on the GPT-2 generated text.
3. Compare the performances of both classifiers.
   - e.g. in (1) you have 60% accuracy and (2) you have 54% accuracy.
   - → This could mean that the New Zealand politicians are not well distinguishable.

**For each classification:**

1. Lemmatize and remove stop words
2. Feature creation
   1. TFIDF
   2. Word2vec with TFIDF weights
3. Standard scaling
4. Feature selection
   1. Select from <u>SVM</u> model
5. Classification with SVC

## Questions to Elliott

- How to argue intuitive/small decisions?
  - e.g. we deleted texts with <32 characters for preprocessing question-dataset
- How to filter personal / contextless questions which cannot be answered properly
  - Another idea from Elliotts side?
- How to handle terms/words that appear very often? procedural
  - e.g. "member", "I raise a point of order", …
  - Another idea from Elliotts side?

## Feedback from Claudia

- "The only thing that you might want to <u>check before the pre-processing is whether the topics of questions and non-questions are balanced across parties</u>."
- Her remark to classifier:
  She thinks it's more suited to use the initial non-questions to train the model; and then test on the GPT output.


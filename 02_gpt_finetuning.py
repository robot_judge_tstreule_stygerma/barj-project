import pandas as pd
import gpt_2_simple as gpt2

file_name = "inferred/01_preprocessing/gpt_finetune.txt"
model_name = "355M"
sess = gpt2.start_tf_sess()
gpt2.finetune(sess,
              file_name,
              model_name=model_name,
              steps=200,
              run_name="NZ_Parliament_QA",
              restore_from='latest',
              max_checkpoints=20,
              print_every=2,
              save_every=200,
              sample_every=200,
              sample_length=256,
              multi_gpu=True)                               